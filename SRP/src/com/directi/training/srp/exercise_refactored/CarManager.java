package com.directi.training.srp.exercise_refactored;

public class CarManager
{
    private final CarFinder _carFinder;
    private final CarFormatter _carFormatter;
    private final CarMarketingManager _CarMarketingManager;

    public CarManager(CarFinder carFinder, CarFormatter carFormatter, CarMarketingManager CarMarketingManage)
    {
        _carFinder = carFinder;
        _carFormatter = carFormatter;
        _CarMarketingManager = CarMarketingManage;
    }

    public Car getCarById(final String carId)
    {
        return _carFinder.getFromDb(carId);
    }

    public String getCarsNames()
    {
        return _carFormatter.getCarsNames(_carFinder.findAll());
    }

    public Car getBestCar()
    {
        return _CarMarketingManager.getBestCar(_carFinder.findAll());
    }
}
