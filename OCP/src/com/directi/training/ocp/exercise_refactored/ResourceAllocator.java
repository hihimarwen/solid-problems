package com.directi.training.ocp.exercise_refactored;

public class ResourceAllocator
{
    
    public int allocate(ResourceType ResourceType)
    {
        int resourceId = ResourceType.findFree();
        ResourceType.markBusy(resourceId);
        return resourceId;
    }

    public void free(ResourceType ResourceType, int resourceId)
    {
        ResourceType.markFree(resourceId);
    }
}
