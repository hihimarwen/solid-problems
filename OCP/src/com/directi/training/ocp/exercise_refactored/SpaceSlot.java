package com.directi.training.ocp.exercise_refactored;

public class SpaceSlot implements ResourceType
{
    
    public int findFree()
    {
        return 0;
    }

    
    public void markBusy(int resourceId)
    {
    }

    public void markFree(int resourceId)
    {
    }
}
